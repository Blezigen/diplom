﻿using Diplom.Logic.Common;
using Diplom.Logic.Common.Enumerations;

namespace Diplom.Logic
{
    public class HistoryData
    {
        public Node DumpedTree { get; set; }
        public string Comment { get; set; }
        public int TempActionValue { get; set; }

        private static Node DumpTree(Node node, Node parentNode = null)
        {
            if (node == null) return null;

            var parent = new Node(node.Data)
            {
                Parent = parentNode,
                LeftChild = DumpTree(node.LeftChild, parentNode),
                RightChild = DumpTree(node.RightChild, parentNode),
                Position = node.Position,
                NodeStatus = node.NodeStatus
            };

            return parent;
        }

        public HistoryData(Node node, string comment = "")
        {
            DumpedTree = DumpTree(node);
            Comment = comment;
        }
    }
}