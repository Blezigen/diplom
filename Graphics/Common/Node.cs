﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Diplom.Logic.Attributes;
using Diplom.Logic.Common.Enumerations;
using Diplom.Logic.Drawing;
using Diplom.Logic.Drawing.DrawingOption;
using Diplom.Logic.Extensions;

namespace Diplom.Logic.Common
{
    /// <summary>
    /// Узел дерева
    /// </summary>
    public class Node
    {
        public int? Data { get; set; }
        public string ArrowText { get; set; }
        public Vector Position
        {
            get => _pos;
            set => _pos = value;
        }

        public NodeStatus NodeStatus { get; set; } = NodeStatus.None;

        public Node Parent { get; set; }
        public Node LeftChild { get; set; }
        public Node RightChild { get; set; }

        public DrawingBinaryTreeOption DrawingOption { get; set; } = new DrawingBinaryTreeOption();

        private Vector _pos;

        public Node()
        {
            _pos = new Vector();
        }

        public NodeStatusAttribute GetStatusAttribute()
        {
            return NodeStatus.GetAttributeOfType<NodeStatusAttribute>();
        }

        public void ClearStatus()
        {
            NodeStatus = NodeStatus.None;
        }

        public Node(int? data) : this()
        {
            Data = data;
        }

        public Node(int? data, Node leftChild, Node rightChild, Node parent = null) : this(data)
        {
            LeftChild = leftChild;
            RightChild = rightChild;
            Parent = parent;
        }

        public void Clear(Node node)
        {
            node.Data = null;
            node.LeftChild = null;
            node.RightChild = null;
            node.Parent = null;
        }

        public NodeSide GetSide()
        {
            if (Parent == null) return NodeSide.Root;
            if (Parent.LeftChild == this) return NodeSide.Left;
            if (Parent.RightChild == this) return NodeSide.Right;
            return NodeSide.None;
        }

        public void PositionNode(ref double xmin, double ymin)
        {
            _pos.Y = ymin + (int) (DrawingOption.NodeSize.Y / 2);

            // Position our left subtree.
            LeftChild?.PositionNode(ref xmin, ymin + DrawingOption.NodeSize.Y + DrawingOption.VerticalMargin);

            // If we have two children, allow space between them.
            if ((LeftChild != null) && (RightChild != null)) xmin += DrawingOption.HorizontalMargin;

            // Position our right subtree.
            RightChild?.PositionNode(ref xmin, ymin + DrawingOption.NodeSize.Y + DrawingOption.VerticalMargin);

            // Position ourself.
            if (LeftChild != null)
            {
                if (RightChild != null)
                {
                    // Center between our children.
                    _pos.X = (int) ((LeftChild._pos.X + RightChild._pos.X) / 2);
                }
                else
                {
                    // No right child. Center over left child.
                    _pos.X = LeftChild._pos.X;
                }
            }
            else if (RightChild != null)
            {
                // No left child. Center over right child.
                _pos.X = RightChild._pos.X;
            }
            else
            {
                // No children. We//re on our own.
                _pos.X = (int) (xmin + DrawingOption.NodeSize.X / 2);
                xmin += DrawingOption.NodeSize.X;
            }
        }

        public void DrawBranches(ref Canvas gr)
        {
            int countChild = (LeftChild == null ? 0 : 1) + (RightChild == null ? 0 : 1);
            Vector offset = new Vector(DrawingOption.NodeSize.X / 5, 0);
            Vector topOffsetVector = new Vector(0, DrawingOption.NodeSize.Y / 2);
            Vector bottomOffsetVector = new Vector(0, (-DrawingOption.NodeSize.Y / 2));
            Vector leftOffset = new Vector(DrawingOption.NodeSize.X / 4, 0);

            if (LeftChild != null)
            {
                var l = LeftChild.GetStatusAttribute();
                var colorLeft = new SolidColorBrush(l.BorderColor);

                if (countChild == 1)
                    new DrawingArrow().DrawArrow(ref gr,
                        new Vector(_pos.X, _pos.Y) - offset + topOffsetVector,
                        new Vector(LeftChild._pos.X, LeftChild._pos.Y) - offset + bottomOffsetVector,
                        LeftChild.ArrowText,
                        new DrawingArrowOption {Side = DrawingEnumArrowSide.Left, ColorThikness = colorLeft});
                else
                    new DrawingArrow().DrawArrow(ref gr,
                        new Vector(_pos.X, _pos.Y) + topOffsetVector - leftOffset,
                        new Vector(LeftChild._pos.X, LeftChild._pos.Y) + bottomOffsetVector,
                        LeftChild.ArrowText,
                        new DrawingArrowOption {ColorThikness = colorLeft});

                LeftChild.DrawBranches(ref gr);
            }

            if (RightChild != null)
            {
                var r = RightChild.GetStatusAttribute();
                var colorRight = new SolidColorBrush(r.BorderColor);

                if (countChild == 1)
                    new DrawingArrow().DrawArrow(ref gr,
                        new Vector(_pos.X, _pos.Y) + offset + topOffsetVector,
                        new Vector(RightChild._pos.X, RightChild._pos.Y) + offset + bottomOffsetVector,
                        RightChild.ArrowText,
                        new DrawingArrowOption {Side = DrawingEnumArrowSide.Right, ColorThikness = colorRight});
                else
                    new DrawingArrow().DrawArrow(ref gr,
                        new Vector(_pos.X, _pos.Y) + topOffsetVector + leftOffset,
                        new Vector(RightChild._pos.X, RightChild._pos.Y) + bottomOffsetVector,
                        RightChild.ArrowText,
                        new DrawingArrowOption {ColorThikness = colorRight});

                RightChild.DrawBranches(ref gr);
            }
        }

        public void DrawNode(ref Canvas gr)
        {
            Vector vectorPos = new Vector(_pos.X - (DrawingOption.NodeSize.X / 2),
                _pos.Y - (DrawingOption.NodeSize.Y / 2));

            var attr = GetStatusAttribute();

            var node = new DrawingNode
            {
                GeneralText = Data.ToString(),
                LeftChildText = LeftChild?.Data.ToString(),
                RightChildText = RightChild?.Data.ToString()
            };

            var s = node.GetDrawingNode(vectorPos, new DrawingNodeOption
                {
                    BorderBrush = new SolidColorBrush(attr.BorderColor),
                    BackgroundBrush = new SolidColorBrush(attr.BackgroundColor),
                    ForegroundBrush = new SolidColorBrush(attr.ForegroundColor),
                });

            gr.Children.Add(s);

            LeftChild?.DrawNode(ref gr);
            RightChild?.DrawNode(ref gr);
        }
    }
}