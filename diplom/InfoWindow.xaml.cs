﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Diplom.Logic.Attributes;
using Diplom.Logic.Common;
using Diplom.Logic.Extensions;

namespace Diplom
{
    public class ButtonClickEventArgs : EventArgs
    {
        public string Text { get; set; }
        public TextBlock StepTextBlock { get; set; }
        public TextBlock StepInfoTextBlock { get; set; }

        public Button PrevStepButton { get; set; }
        public Button NextStepButton { get; set; }

        public Grid ImageOutput { get; set; }
    }
    /// <summary>
    /// Логика взаимодействия для InfoWindow.xaml
    /// </summary>
    public partial class InfoWindow : Window
    {
        private Tree trees;
        private int _historyStep = 1;
        public event EventHandler ButtonClickEvent;

        public InfoWindow()
        {
            InitializeComponent();
          
        }

        public InfoWindow(ref Tree trees) 
        {
            InitializeComponent();
            this.trees = trees;
            UpdateWindow();
        }

        private void UpdateWindow()
        {
            PrevStepButton.IsEnabled = false;
            NextStepButton.IsEnabled = false;
            InfoTextBlock.Foreground = new SolidColorBrush(Colors.Black);

            if (trees.HistoryCount() > 0 & _historyStep > 1)
                PrevStepButton.IsEnabled = true;

            if (_historyStep < trees.HistoryCount())
                NextStepButton.IsEnabled = true;

            if (_historyStep == trees.HistoryCount())
            {
                var s = trees.GetHistory(_historyStep);
                //var attr = s.NodeStatus.GetAttributeOfType<NodeStatusAttribute>();
                //InfoTextBlock.Foreground = new SolidColorBrush(attr.ForegroundColor);
            }

            TextBlock.Text = $"Шаг #{_historyStep + 1}";

            trees.ApplyHistory(_historyStep);
            InfoTextBlock.Text = $"{trees.GetHistory(_historyStep).Comment}";
        }

        private void Button_Click_Prev(object sender, RoutedEventArgs e)
        {
            if (_historyStep > 1)
                _historyStep--;
            OnButtonClickEvent();
        }

        private void Button_Click_Next(object sender, RoutedEventArgs e)
        {
            if (_historyStep < trees.HistoryCount())
                _historyStep++;
            OnButtonClickEvent();
        }

        protected virtual void OnButtonClickEvent()
        {
            UpdateWindow();
            ButtonClickEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}